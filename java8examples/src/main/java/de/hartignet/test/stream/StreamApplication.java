package de.hartignet.test.stream;

import lombok.extern.java.Log;

import java.util.List;
import java.util.stream.Collectors;

@Log
public class StreamApplication {

    public static void main(String[] args) {

        List<StreamRoot> roots = StreamFactory.createRootList(5, 5, 5);
        //log.info("allRoots = " + roots);
        //  log.info("root1 = " + getRoot(roots, 1));
        //      log.info("root6 = " + getRoot(roots, 6));
        //    log.info("root1 exists? = " + exists(roots, 1));
        //  log.info("root6 exists? = " + exists(roots, 6));
        //       log.info("childs = " + getChilds(roots));
//        log.info("important childs = " + getImportantChilds(roots));
        log.info("priceSum = " + getPriceSum(roots));
    }

    private static StreamRoot getRoot(List<StreamRoot> roots, int id) {

        return roots.stream().filter(root -> root.getId() == id).findFirst().orElse(null);
    }

    private static boolean exists(List<StreamRoot> roots, int id) {

        return roots.stream().peek(item -> log.info(item.toString())).anyMatch(root -> root.is(id));
    }

    private static List<StreamChild> getChilds(List<StreamRoot> roots) {

        return roots.stream().map(StreamRoot::getChilds).flatMap(List::stream).collect(Collectors.toList());
    }

    private static List<StreamChild> getImportantChilds(List<StreamRoot> roots) {

        return roots.stream()
                    .map(StreamRoot::getChilds)
                    .flatMap(List::stream)
                    .filter(StreamChild::isImportant)
                    .collect(Collectors.toList());
    }

    private static Integer getPriceSum(List<StreamRoot> roots) {

        return roots.parallelStream()
                    .map(StreamRoot::getChilds)
                    .flatMap(List::stream)
                    .filter(StreamChild::isImportant)
                    .map(StreamChild::getGrandchilds)
                    .flatMap(List::stream)
                    .map(StreamGrandchild::getPrice)
                    .reduce(0, (a, b) -> a + b);
    }

    private static Integer getPriceSumOldschool(List<StreamRoot> roots) {

        Integer sum = 0;
        for (StreamRoot root : roots) {
            for (StreamChild child : root.getChilds()) {
                if (child.isImportant()) {
                    for (StreamGrandchild grandchild : child.getGrandchilds()) {
                        sum = sum + grandchild.getPrice();
                    }
                }
            }
        }
        return sum;
    }
}
