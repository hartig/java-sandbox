package de.hartignet.test.stream;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamFactory {

    public static List<StreamRoot> createRootList(int size, int childSize, int grandchildSize) {

        return Stream.iterate(1, n -> n + 1).limit(size)
                     .map(it -> createRoot(it, childSize, grandchildSize))
                     .collect(Collectors.toList());
    }

    private static StreamRoot createRoot(int index, int childSize, int grandchildSize) {

        StreamRoot.StreamRootBuilder builder = StreamRoot.builder().id(index);
        for (int childIndex = 1; childIndex <= childSize; childIndex++) {
            builder = builder.child(createStreamChild((index - 1) * childSize + childIndex, grandchildSize));
        }
        return builder.build();
    }

    private static StreamChild createStreamChild(int index, int childSize) {

        StreamChild.StreamChildBuilder builder = StreamChild.builder().id(index).important(index % 2 == 0);
        for (int childIndex = 1; childIndex <= childSize; childIndex++) {
            builder = builder.grandchild(createStreamGrandchild((index - 1) * childSize + childIndex));
        }
        return builder.build();
    }

    private static StreamGrandchild createStreamGrandchild(int index) {

        return StreamGrandchild.builder().id(index).price(createRandomInt()).build();
    }

    private static int createRandomInt() {

        return ThreadLocalRandom.current().nextInt(1, 5 + 1);
    }
}
