package de.hartignet.test.stream;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@Builder
public class StreamChild {

    private int id;

    private boolean important;

    @Singular
    private List<StreamGrandchild> grandchilds;
}
