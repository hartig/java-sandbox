package de.hartignet.test.stream;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@Builder
public class StreamRoot {

    private int id;

    @Singular
    private List<StreamChild> childs;

    public boolean is(int id) {

        return this.id == id;
    }
}
