package de.hartignet.test.stream;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class StreamGrandchild {

    private int id;

    private int price;
}
