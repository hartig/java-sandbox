package de.hartignet.test.function;

public interface TestInterface2 {

    void testMethod();

    default void testMethod2() {
        System.out.println("Hello Daniel!");
    }
}
