package de.hartignet.test.function;

@FunctionalInterface
public interface TestInterface {

    void testMethod();

    default void testMethod2() {
        System.out.println("Hello World!");
    }
}
