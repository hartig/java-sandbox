package de.hartignet.test.function;

import lombok.extern.java.Log;

import java.util.function.Function;

@Log
public class FunctionClass implements Function<Integer, Integer> {

    public Integer apply(Integer integer) {
        log.info("Die Zahl lautet: " + integer);
        return integer;
    }
}
