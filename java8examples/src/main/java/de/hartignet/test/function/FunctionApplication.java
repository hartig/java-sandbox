package de.hartignet.test.function;

import lombok.extern.java.Log;

import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log
public class FunctionApplication {

    Supplier<Object> supplier;
    Consumer<Object> consumer;
    Predicate<Object> predicate;
    Function<Object, Object> function;

    BiConsumer<Object, Object> biConsumer;
    BiPredicate<Object, Object> biPredicate;
    BiFunction<Object, Object, Object> biFunction;

    UnaryOperator<Object> unaryOperator;
    BinaryOperator<Object> binaryOperator;


    public static void main(String[] args) {

        // Eigene Klasse
        new FunctionClass().apply(1);

        // anonyme Klasse
        new Function<Integer, Integer>() {
            public Integer apply(Integer integer) {
                log.info("Die Zahl lautet: " + integer);
                return integer;
            }
        }.apply(2);

        // Lambda
        ((Function<Integer, Integer>) integer -> {
            log.info("Die Zahl lautet: " + integer);
            return integer;
        }).apply(3);

        // Lambda in Stream I
        createStream().map(integer -> {
            log.info("Die Zahl lautet: " + integer);
            return integer;
        }).collect(Collectors.toList());

        // Lambda in Stream II
        createStream().map(integer -> {
            return logAndReturn(integer);
        }).collect(Collectors.toList());

        // Lambda in Stream III
        createStream().map(integer -> logAndReturn(integer)).collect(Collectors.toList());

        // Lambda in Stream IV
        createStream().map(fun).collect(Collectors.toList());

        // Lambda in Stream V
        createStream().map(FunctionApplication::logAndReturn).collect(Collectors.toList());
    }

    private static Function<Integer, Integer> fun = integer -> logAndReturn(integer);

    private static Integer logAndReturn(Integer integer) {
        log.info("Die Zahl lautet: " + integer);
        return integer;
    }

    private static Stream<Integer> createStream() {
        return Stream.iterate(4, n -> n + 1).limit(10);
    }
}
